var path;
var host = "http://192.168.0.102:3000";
var server = host + "/api";
var dataObject;
var question = $.cookie("question-one");
var hash = $.cookie("hash");
var userId = $.cookie("user-id");
console.log(userId);
$('body').click(function(e) {

    var target = $(e.target), article;

    if(target.is('#mic')) {
       $("#mic").toggleClass('fa-microphone-slash');
    }
    else if (target.is('#ans1')) {
    	path = host + "/api/add-new-answer";
    	dataObject = {
    				"userId":userId,
    				"qNum": question,
    				"hash":hash,
					"qAns":1
					 };
		answerClick(dataObject);
    }
    else if (target.is('#ans2')) {
    	path = host + "/api/add-new-answer";
    	dataObject = {
    				"userId":userId,
    				"qNum": question,
    				"hash":hash,
					"qAns":2
					 };
		answerClick(dataObject);
    }
    else if (target.is('#ans3')) {
    	path = host + "/api/add-new-answer";
    	dataObject = {
    				"userId":userId,
    				"qNum": question,
    				"hash":hash,
					"qAns":3
					 };
		answerClick(dataObject);
    }
    else if (target.is('#ans4')) {
    	path = host + "/api/add-new-answer";
    	dataObject = {
    				"userId":userId,
    				"qNum": question,
    				"hash":hash,
					"qAns":4
					 };
		answerClick(dataObject);
    }
    if (target.is('#accept')) {
    	path = host + "/api/notifications/qNum/0";
    	window.open('http://stackoverflow.com/', '_blank');
    };
    if (target.is('#next')) {
    	dataObject = {
    				"qNum": question,
    				"hash":hash,
					"qAns":4
					};
    	getQuestion(dataObject);
    };
});

function answerClick(ans) {
	$.ajax({
        url:path,
        method:"POST",
        data:JSON.stringify(ans),
        success : function (data){
            console.log(data);
        },
        error : function (e){
        	console.log(e);
        },
        dataType:"json",
        contentType:"application/json"
	});
}
$(document).ready(function(){          
    $.ajax({ url: server+"/notifications/hash/"+hash, success: function(data) {
		console.log(data);
	}
	});
});

function getQuestion(ans) {
	$.ajax({
        url:server+"/get-active-question",
        method:"POST",
        data:JSON.stringify(ans),
        success : function (data){
            console.log(data);
        },
        error : function (e){
        	console.log(e);
        },
        dataType:"json",
        contentType:"application/json"
	});
}
(function poll(){

	setTimeout(function() {
		$.ajax({ url: server+"/notifications/", success: function(data){
			console.log(data[0]["state"]);
			if (data[0]["state"] == 0) {	
				$.notify({
	                icon: 'pe-7s-flag',
	                message:  data[0]["message"]+"<i id='accept'class='pe-7s-like2' style='float:right;'></i>"
	            },{
	                type: 'success',
	                timer: 800
	            });
			}
			}, dataType: "json", complete: poll, timeout: 30000 });
	}, 5000);
})();

var user = {};
var arr1 = [];
var arr2 = [];
var arr3 = [];
var arr4 = [];

(function poll(){
	
	setTimeout(function() {
		$.ajax({ 
			url: server+"/answer-tags/"+hash+"/"+question, 

			success: function(data){
				//console.log(data);
				var htmlStr = "";

				for(var i = 0; i < data.length; i++) {
					//console.log(data[i]["fName"]);
					htmlStr += "<span class='label label-default " + data[i]["fName"] + "' style='display:none; background-color:#926ED3;'>"+data[i]["fName"]+"</span>&nbsp";
				}

				//console.log(htmlStr);
				$("#user1").html(htmlStr);
				$("#user2").html(htmlStr);
				$("#user3").html(htmlStr);
				$("#user4").html(htmlStr);

				for(var i = 0; i < data.length; i++) {
					//console.log(data[i]["fName"]);
					//console.log(data[i]["qAns"]);
					if(data[i]["qAns"] == 1) {
						$("#user1").find("."+data[i]["fName"]).css("display", "inline-block");
					}
					if(data[i]["qAns"] == 2) {
						$("#user2").find("."+data[i]["fName"]).css("display", "inline-block");
					}
					if(data[i]["qAns"] == 3) {
						$("#user3").find("."+data[i]["fName"]).css("display", "inline-block");
					}
					if(data[i]["qAns"] == 4) {
						$("#user4").find("."+data[i]["fName"]).css("display", "inline-block");
					}

				}				

			}, 
			dataType: "json", 
			complete: poll, 
			timeout: 30000 
		});
	}, 2000);
})();


//data change 
//<span class="label label-default" style="background-color:#926ED3;">Saad</span>&nbsp