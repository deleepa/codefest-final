var host = "http://192.168.0.102:3000";
var id = $.cookie("user-id");

$(document).ready(function() {
	getClans();
});

function getClans() {
	var count = 0;
	$.ajax({
		url: host+"/api/match-clan/1",
		method: "GET",
		success: function(data) {
			$.each(data, function(key, value) {
				$.ajax({
					url: host+"/api/clan/clanId/"+key,
					method: "GET",
					success: function(dat) {
						if(count != 0 ){
						var img;
						if(value == "online") {
							img = "<img class='img-responsive' src='img/online.png'>";
							$("#clanTable").append("<tr><td>"+dat[0]["clanName"]+"</td><td>"+img+"</td><td><input type='radio' name='clanSelect' value='"+key+"'></td></tr>");
						}
						else{
							img = "<img class='img-responsive' src='img/offline.png'>";
							$("#clanTable").append("<tr><td>"+dat[0]["clanName"]+"</td><td>"+img+"</td><td><input type='radio' disabled name='clanSelect' value='"+key+"'></td></tr>");
						}
						
						}
						count++;
					},
					error: function(e) {
						console.log(e);
					}
				});
			});
		}
	});
}

$("body").click(function(e) {
	var radioValue = null;
	$("input[type='radio']").click(function(){
            radioValue = $("input[name='clanSelect']:checked").val();

            if(radioValue != null) {
            	$('button').prop('disabled', false);
            }
    });

	var data = {
		"userId" : id
	}

	$("#selectClan").click(function() {
		$.ajax({
			url: host+"/api/start-clan-game",
			method: "POST",
			data: JSON.stringify(data),
			dataType:"json",
			contentType:"application/json",
			success: function(data) {
				window.location.href = host+"/clan-quiz";
			}
		});
	});
});