var host = "http://192.168.0.102:3000";
var id = $.cookie("user-id");
var email;

$(document).ready(function() {
	getUsermail(id);
	getUserRankings();
	getRankings();
});

function getUserRankings() {
	var uniId;
	var clanId;

	$.ajax({
		url: host+"/api/user/e-mail/"+email,
		method: "GET",
		success: function(data) {
			$("#indRank").html(data[0]["userRating"]);
			uniId = data[0]["uniId"];
			clanId = data[0]["clanId"];
			
			$.ajax({
				url: host+"/api/clan/clanId/"+clanId,
				method: "GET",
				success: function(data) {
					$("#clanRank").html(data[0]["clanRating"]);
				}
			});

			$.ajax({
				url: host+"/api/university/uniId/"+uniId,
				method: "GET",
				success: function(data) {
					$("#uniRank").html(data[0]["uniRating"]);
				}
			});

		}
	});
}

function getRankings() {

	$.ajax({
		url: host+"/api/user/e-mail/"+email,
		method: "GET",
		success: function(dat) {
			var uniId = dat[0]["uniId"];
			var clanId = dat[0]["clanId"];
			var user = email;

			$.getJSON(host+"/api/user", function(data) {
				var values = _.sortBy(data, function(item) { return -item.userRating; })
				var items= [];
				for (var i = 0; i < values.length; i++) {
					if(user == values[i]["e-mail"]) {
						items.push("<tr style='background-color:#dccef6;'><td style='font-weight:bold;'>"+values[i]["fName"]+"</td><td style='font-weight:bold;'>"+values[i]["userRating"]+"</td></tr>");
					}
					else{
						items.push("<tr><td>"+values[i]["fName"]+"</td><td>"+values[i]["userRating"]+"</td></tr>");
					}
				};
				$("#indRankings").append(items);
			});

			$.getJSON(host+"/api/clan", function(data) {
				var values = _.sortBy(data, function(item) { return -item.clanRating; })
				var items= [];
				for (var i = 0; i < values.length; i++) {
					if(clanId == values[i]["clanId"]) {
						items.push("<tr style='background-color:#dccef6;'><td style='font-weight:bold;'>"+values[i]["clanName"]+"</td><td style='font-weight:bold;'>"+values[i]["clanRating"]+"</td></tr>");
					}
					else {
						items.push("<tr><td>"+values[i]["clanName"]+"</td><td>"+values[i]["clanRating"]+"</td></tr>");
					}
				};
				$("#clanRankings").append(items);
			});

			$.getJSON(host+"/api/university", function(data) {
				var values = _.sortBy(data, function(item) { return -item.uniRating; })
				var items= [];
				for (var i = 0; i < values.length; i++) {
					if (uniId == values[i]["uniId"]) {
						items.push("<tr style='background-color:#dccef6;'><td style='font-weight:bold;'>"+values[i]["uniName"]+"</td><td style='font-weight:bold;'>"+values[i]["uniRating"]+"</td></tr>");
					}
					else {
						items.push("<tr><td>"+values[i]["uniName"]+"</td><td>"+values[i]["uniRating"]+"</td></tr>");
					}
				};
				$("#uniRankings").append(items);
			});

		}
	});

}

function getUsermail(e){
    console.log(e);
    $.ajax({
        url:host+"/api/user/id/"+e+"",
        method : "GET",
        success : function(data){
            console.log(data);
            email = data[0]["e-mail"];
            console.log(email);
        },
        error : function (err){
            console.log(err);
        },
        dataType:"json",
        contentType:"application/json"
    })
}