var host = "http://192.168.0.102:3000";
var user ;
var correct;

var today = new Date();
var tod = today.getFullYear()+"-"+(today.getMonth()+1)+"-0"+(today.getDay()+1);
$(document).ready(function(){
	console.log(document.cookie);
	$("#example-timer").circletimer({
		timeout:30000,
		onComplete:(function(){
			updateDailyQuestion(0);
			console.log('time up');
			$("#daily-question").modal('hide');
		}),


	})
	var userId = $.cookie("user-id");
	console.log(userId);
	getUserEmail(userId);
	window.setTimeout(loadDailyQuestion(), 50000)
});

function loadDailyQuestion(){	
	var data = {
		"email":user,
		"date": tod
	}
	console.log(tod);
	$.ajax({
		url:host+"/api/check-daily-question/",
		method:"POST",
		data : JSON.stringify(data),
		success : function(dat){
			console.log(dat[0]);
			if(dat[0] == undefined){
				getQuestion();
			}
		},
		error : function(e){

			console.log(e);
		},
		dataType:"json",
		contentType:"application/json"
	});

	
}

function getQuestion(){
	var id = Math.random();
	console.log("follows random");
	var val = parseInt(id*10);
	$.ajax({
		url:host+"/api/dailyquestion/dailyQId/"+val+"",
		method : "GET",
		success : function(data){
			console.log(data[0].dailyQ);
			document.getElementById("question").innerHTML = data[0].dailyQ;
			document.getElementById("ans1").innerHTML = data[0].dailyAnswer1;
			document.getElementById("ans1").name = data[0].dailyAnswer1;
			document.getElementById("ans2").innerHTML = data[0].dailyAnswer2;
			document.getElementById("ans2").name = data[0].dailyAnswer2;
			document.getElementById("ans3").innerHTML = data[0].dailyAnswer3;
			document.getElementById("ans3").name = data[0].dailyAnswer3;
			correct = data[0].dailyCorrectAnswer;
			$("#daily-question").modal("show");
			$("#example-timer").circletimer("start");

		}

	})
}
function addPoints (){
	$.ajax({
		url:host+"/api/user/e-mail/"+user+"",
		method:"GET",
		success : function (data){
			var rating = parseInt(_.pluck(data,"userRating"));
			incrementRating(rating);
		},
		error : function(e){
				console.log(e);
		},
		dataType:"json",
		contentType:"application/json"

	})
}
function incrementRating(rating){
	var data = {
		"userRating":rating+10
	}
	console.log(data);
	$.ajax({
		url : host+"/api/user/e-mail/"+user+"",
		method : "POST",
		data : JSON.stringify(data),
		success : function (e){
			console.log(e);
			updateDailyQuestion(10)
			getRankings();
		},
		error : function (e){
			console.log(e);
		},
		dataType:"json",
		contentType:"application/json"

	})

}
$(".ans").click(function(e){	
	var answer = e.toElement.name;
	$("#daily-question").modal("hide");
	if(answer == correct){		
		addPoints();
	}else{
		updateDailyQuestion(0);
	}

})

function updateDailyQuestion(e){
	var data = {
		"user-email":user,
		"date":tod,
		"answered":1,
		"rating":e
	}

	$.ajax({
		url:host+"/api/trackdailyquestion",
		method : "PUT",
		data : JSON.stringify(data),
		success : function (e){
			console.log(e);
		},
		error : function (e){
			console.log(e);
		},
		dataType:"json",
		contentType:"application/json"
	})
}

function getUserEmail(e){
	console.log(e);
	$.ajax({
		url:host+"/api/user/id/"+e+"",
		method : "GET",
		success : function(data){
			console.log(data);
			user = data[0]["e-mail"];
			console.log(user);
		},
		error : function (err){
			console.log(err);
		},
		dataType:"json",
		contentType:"application/json"
	})
}

