"use strict";

var router;
var path = require("path");

function setRouter(r) {
	console.log("Setting up router..");
	router = r;
}

function getRouter() {
	if(router == undefined) {
		throw new Error("You must define a router first. Call setRouter()");
	}
	else {
		return router;
	}
}

function buildRoutes() {
	if(router == undefined) {
		throw new Error("You must define a router first. Call setRouter()");
	}
	else {
		router.get("/", function(req, res) {
			res.status(200).sendFile(path.resolve("./public/views/user.html"));
		});

		router.get("/clan-quiz", function(req, res) {
			res.status(200).sendFile(path.resolve("./public/views/clan-quiz.html"));
		});

		router.get("/table", function(req, res) {
			res.status(200).sendFile(path.resolve("./public/views/table.html"));
		});


		router.get("/profile", function(req, res) {
			res.status(200).sendFile(path.resolve("./public/views/profile.html"));
		});

		router.get("/dashboard", function(req, res) {
			res.status(200).sendFile(path.resolve("./public/views/index.html"));
		});
	}	
}

exports.setRouter = setRouter;
exports.getRouter = getRouter;
exports.buildRoutes = buildRoutes;