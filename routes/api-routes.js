"use strict";

var connection;
var router;
var crypto = require("crypto-js/sha256");
var mysql = require("mysql");
var async = require("async");

function setRouter(r) {
	router = r;
}

function getRouter() {
	
	if(router == undefined) {
		throw new Error("router must be defined first. Call setRouter().");
	}
	else {
		return router;	
	}
	
}

function buildRoutes() {

	if(router == undefined) {
		throw new Error("router must be defined first. Call setRouter().");
	}
	else {

		router.get("/", function(req, res) {
			res.send("hello world.. from api routes..");
		});

		router.get("/chamara-test/:val", function(req, res) {

			if(req.params.val == 1 || req.params.val == 2 || req.params.val == 3 || req.params.val == 4) {
				res.status(200).json({"val" : req.params.val });	
			}

			
		});


		router.post("/user-login", function(req, res) {

			var query = "SELECT * FROM `user` WHERE `e-mail`='" + req.body.email + "' AND `password`='" + req.body.password + "';";

			console.log(query);

			queryRunner(query, function(status, result) {
				if(status) {
					console.log(result.length);
					if(result.length != 0) { //valid user was found
						//set status to online
						res.cookie("user-id", result[0]["id"]);
						res.cookie("email", result[0]["e-mail"]);
						var returnObj = {
							"status" : "true",
							"userStatus" : result[0]["status"] 
						};

						query = "UPDATE `user` SET `status`='online' WHERE `id`='" + result[0]["id"] + "';";	
						queryRunner(query, function(status, result) {
							if(status) {
								res.status(200).send(returnObj);
							}
							else {
								res.status(200).json({"status":"false", "message" : "Could't update user status. Internal Error"});			
							}
						});
					}
					else {
						var returnObj = {
							"status" : "false",
							"message" : "User not found. Invalid credentials"
						};

						res.status(200).send(returnObj);	
					}
				}
				else {
					res.status(500).send(result);
				}
			});
		});

		router.post("/check-daily-question", function(req, res) {
			var reqEmail = req.body.email;
			var reqDate = req.body.date;

			var query = "SELECT * FROM `trackdailyquestion` where `user-email`='"+reqEmail+"' AND `date`='"+reqDate+"';";
			console.log(query);
			queryRunner(query, function(status, result) {
				if(status) {
					res.status(200).send(result);	
				}
				else {
					res.status(500).send(result);	
				}
			});
		});

		router.post("/start-clan-game", function(req, res) {
			//1. generate a hash with current time and clan id

			var hash = crypto(req.body.userId + Date.now());
			console.log(hash);
			//2. get the current user's clanId
			var query = "SELECT `clanId` FROM `user` WHERE `id`='" + req.body.userId + "';";

			queryRunner(query, function(status, result) {
				if(status) {
					query = "SELECT * FROM `user` WHERE `clanId`='" + result[0]["clanId"] + "';";
					
					queryRunner(query, function(status, result) {
						if(status) {

							async.each(result, function(user, callback) {
								console.log(user);
								query = "INSERT INTO `questions` (`hash`, `userId`, `qNum`) VALUES ('"+hash+"', '"+ user["id"] +"', '1');" +
										"INSERT INTO `questions` (`hash`, `userId`, `qNum`) VALUES ('"+hash+"', '"+ user["id"] +"', '2');" +
										"INSERT INTO `questions` (`hash`, `userId`, `qNum`) VALUES ('"+hash+"', '"+ user["id"] +"', '3');" +
										"INSERT INTO `questions` (`hash`, `userId`, `qNum`) VALUES ('"+hash+"', '"+ user["id"] +"', '4');" +
										"INSERT INTO `questions` (`hash`, `userId`, `qNum`) VALUES ('"+hash+"', '"+ user["id"] +"', '5');";

								queryRunner(query, function(status, results) {
									if(status) {
										console.log("added for userId: " + user["id"]);
										callback();
									}
									else {
										res.status(500).send(result);			
									}
								});


							}, function(err) {
								if(err) {
									console.log(err);
								}
								else {
									var message = {
										"status" : "true",
										"message" : "all done. redirect to?",
									};

									res.status(200).send(message);			
								}
							});

						}
						else {
							res.status(500).send(result);	
						}
					});	
				}
				else {
					res.status(500).send(result);	
				}
			});
		});

		router.post("/get-next-question", function(req, res) {

			var clanQuestionId = Math.random() * 10;

			var query = "UPDATE `questions` SET `clanQuestionId`='"+clanQuestionId+"' WHERE `hash`='"+req.body.hash+"' AND `qNum`='"+req.body.qNum+"';";

			queryRunner(query, function(status, results) {
				if(status) {
					results["clanQuestionId"] = clanQuestionId;
					res.status(200).send(results);
				}
				else {
					res.status(500).send(results);
				}
			});

		});

		router.get("/check-user-online/:id", function(req, res) {
			
			var query = "SELECT * FROM `user` WHERE `id`='" + req.params.id + "';";

			queryRunner(query, function(status, result) {
				if(status) {
					var returnObj = {
						"id" : result[0]["id"], 
						"userStatus" : result[0]["status"] 
					};

					res.status(200).send(returnObj);
				}
				else {
					res.status(500).send(result);
				}
			});
		});

		router.get("/get-clan-members/:id", function(req, res) {
			
			var query = "SELECT * from `user` WHERE `clanId`='" + req.params.id + "';";

			queryRunner(query, function(status, result) {
				if(status) {
					//find other members with same clan id
					console.log(result);

					var returnObj = {
						"clanId" : req.params.id,
						"memberIds" : {}
					};

					for(var i = 0; i < result.length; i++) {

						returnObj["member"][i] = result[i]["id"];

					}

					res.status(200).send(returnObj);
				}
				else {
					res.status(500).send(result);
				}
			});
		});

		router.get("/match-clan/:id", function(req, res) {
			
			//1. get current clan's rating
			var query = "SELECT `clanRating` from `clan` WHERE `clanId`='" + req.params.id + "';";
			queryRunner(query, function(status, result) {
				if(status) {
					//2. find another clan with a similar rating. check clans that are +/- 10
					var clanMinRating = Math.floor(result[0]["clanRating"]/10) * 10;
					var clanMaxRating = Math.ceil(result[0]["clanRating"]/10)  * 10;

					console.log("clanMinRating: " + clanMinRating);
					console.log("clanMaxRating: " + clanMaxRating);

					query = "SELECT * FROM `clan` WHERE `clanRating` BETWEEN " + clanMinRating + " AND " + clanMaxRating + ";";
					
					queryRunner(query, function(status, results) {
						if(status) {
							if(result.length != 0) {
								//console.log("matched clans are:");
								//console.log(results);
								//3. check if all members of that clan are online
								var availableClans = {};

								async.each(results, function(item, callback) {
									//console.log(item);
									//console.log("item processed");
									query = "SELECT * from `user` WHERE `clanId`='" + item.clanId + "';";

									queryRunner(query, function(status, result) {
										if(status) {
											var flag = 0;

											async.each(result, function(item, callback){
												if(item["status"] == "offline") {
													console.log(item["e-mail"] + " is offline");
													flag = 1;
												}
												if(flag == 1) {
													availableClans[item["clanId"]] = "offline";
												}
												else {
													availableClans[item["clanId"]] = "online";	
												}

											}, function(err){
												if(err) {
													console.log(err);	
												}
												else {
													console.log("single clan processed");	
												}
											});
											callback();
										}
										else {
											res.status(500).send(result);	
										}
									});

								}, function(err) {
									if(err) {
										console.log(err);	
									}
									else {

										res.status(200).send(availableClans);	
									}
									
								});
							}
							else {
								var message = {
									"status" : "false",
									"message" : "No suitable clans were found online at this moment."
								};
								res.status(500).send(result);	
							}
						}
						else {
							res.status(500).send(result);
						}
					});

					
				}
				else {
					res.status(500).send(result);
				}
			});

		});

		router.get("/answer-tags/:hash/:qNum", function(req, res) {

			var query = "SELECT `questions`.`qAns`, `user`.`fName` FROM `questions`, `user` WHERE `hash`='"+req.params.hash+"' AND `qNum`='"+req.params.qNum+"' AND `questions`.`userId`=`user`.`id`;";
			//console.log(query);
			queryRunner(query, function(status, result) {
				if(status) {

					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);	
				}
			});
		});

		//GET: select
		//GET: select with param - if additional param is set
		//PUT: insert - with JSON for 'set'
		//POST: update - with JSON for 'set'
		//DELETE: delete
		var statements = require("./statements.json");
		//GET: select

		router.get("/:table", function(req, res) {

			var finalQuery = statements[0];
			finalQuery = finalQuery.replace("TABLE", req.params.table);

			queryRunner(finalQuery, function(status, result) {
				if(status) {
					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);
				}
			});

			//logger.log("info", "req: ", req);

		});

		router.get("/:table/:column/:value", function(req, res) {

			var finalQuery = statements[1];
			finalQuery = finalQuery.replace("TABLE", req.params.table);
			finalQuery = finalQuery.replace("COLUMN", req.params.column);
			finalQuery = finalQuery.replace("?", req.params.value);

			queryRunner(finalQuery, function(status, result) {
				if(status) {
					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);
				}
			});
			console.log("finalQuery: " + finalQuery);
		});

		router.delete("/:table/:column/:value", function(req, res) {
			
			var finalQuery = statements[2];
			finalQuery = finalQuery.replace("TABLE", req.params.table);
			finalQuery = finalQuery.replace("COLUMN", req.params.column);
			finalQuery = finalQuery.replace("?", req.params.value);

			queryRunner(finalQuery, function(status, result) {
				if(status) {
					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);
				}
			});

		});

		router.put("/:table", function(req, res) {
			console.log("req.params.table: " + req.params.table);
			var finalQuery = statements[3];
			finalQuery = finalQuery.replace("TABLE", req.params.table);
			var setData = "";
			var index = 0;

			for(var key in req.body) {
				index++;

				if(index < Object.keys(req.body).length ) {
					setData += "`" + key + "`" + " = '" + req.body[key] + "', ";
				}
				else {
					setData += "`" + key + "`" + " = '" + req.body[key] + "' ";	
				}
			}

			finalQuery = finalQuery.replace("?", setData);

			queryRunner(finalQuery, function(status, result) {
				if(status) {
					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);
				}
			});	
		});

		router.post("/:table/:column/:value", function(req, res) {
			
			var finalQuery = statements[4];
			finalQuery = finalQuery.replace("TABLE", req.params.table);
			finalQuery = finalQuery.replace("COLUMN", req.params.column);
			finalQuery = finalQuery.replace("$", req.params.value);
			var setData = "";
			var index = 0;

			console.log(req.body);

			for(var key in req.body) {
				index++;

				if(index < Object.keys(req.body).length ) {
					setData += "`" + key + "`" + " = '" + req.body[key] + "', ";
				}
				else {
					setData += "`" + key + "`" + " = '" + req.body[key] + "' ";	
				}
			}

			finalQuery = finalQuery.replace("?", setData);

			queryRunner(finalQuery, function(status, result) {
				if(status) {
					res.status(200).send(result);
				}
				else {
					res.status(500).send(result);
				}
			});

		});
	}

}

function createDBConnection() {
	var dbConfig = require("../configs/db-config.json");

	connection = mysql.createConnection({
		host: dbConfig["host"],
		user: dbConfig["username"],
		password: dbConfig["password"],
		database: dbConfig["database"],
		multipleStatements: true
		//debug: ["ComQueryPacket", "RowDataPacket"]
	});
}

function queryRunner(query, callback) {

	if(connection === undefined) {
		createDBConnection();
	}

	connection.escape(query);

	connection.query(query, function(err, results, fields) {
		if(err) {
			callback(false, err);
		}
		else {
			callback(true, results);
		}
	});

}

exports.setRouter = setRouter;
exports.getRouter = getRouter;
exports.buildRoutes = buildRoutes;
