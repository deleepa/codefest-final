"use strict";

var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var path = require("path");
var cors = require("cors");

/* -- Set up the middleware -- */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "PUT");
  next();
});

app.use(bodyParser.json());

/* -- Set up the routes -- */
var apiRoutes = require("./routes/api-routes.js");
var routes = require("./routes/routes.js");

routes.setRouter(express.Router());
apiRoutes.setRouter(express.Router());

routes.buildRoutes();
apiRoutes.buildRoutes();

app.use("/", routes.getRouter());
app.use("/api", apiRoutes.getRouter());
//app.use("/api", apiRoutes.getRouter());

/* -- Set up the static file routes -- */

// -- CSS Routing
app.use("/css/:filename", function(req, res) {
	res.status(200).sendFile(path.resolve("./public/css/" + req.params.filename));
});

// -- JS Routing
app.use("/js/:filename", function(req, res) {
	res.status(200).sendFile(path.resolve("./public/js/" + req.params.filename));
});

// -- Image Routing
app.use("/img/:filename", function(req, res) {
	res.status(200).sendFile(path.resolve("./public/img/" + req.params.filename));
});

// -- JSON Routing
app.use("/json/:filename", function(req, res) {
	res.status(200).sendFile(path.resolve("./public/json/" + req.params.filename));
});

// -- Fonts Routing
app.use("/fonts/:filename", function(req, res) {
	res.status(200).sendFile(path.resolve("./public/fonts/" + req.params.filename));
});

/* -- Start up server -- */
app.listen(3000, function() {
	console.log("Server is listening at port: 3000");
});